library ieee;
use ieee.std_logic_1164.all;

entity process_demo is 
    port (
        btn_in : in std_logic;
          controller : in std_logic;
        led_out: out std_logic
    );
end process_demo;

architecture arch of process_demo is 
begin 
    --led_out <= btn_in;
    P1: process(controller)
        begin 
            if controller'event and controller = '1' then
                led_out <= not btn_in;
            end if;
        end process;
end arch;