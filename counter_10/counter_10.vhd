library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity counter_10 is 
    port (clk : in std_logic;
          --q   : out std_logic_vector(3 downto 0));
          q   : out unsigned(3 downto 0));
end counter_10;

architecture arch of counter_10 is

--signal a, b, c, d : std_logic;
signal q_int : unsigned(3 downto 0);

begin 
    P:process(clk)
    begin 
        if (clk'event and clk = '0') then 
            --a <= not a;
            --b <= ((a and not(b) and not(d)) or (not(a) and b and not(d)));
            --c <= ((a and b and not c) or (not(a) and c) or (not(b) and c and not(d)));
            --d <= ((not(a) and (d)) or (a and b and c));
            q_int <= q_int + 1;
            if (q_int >= "1001") then
                q_int <= "0000";
            end if;
        end if;        
    end process P;


    q <= q_int;
    --q(0) <= a;
    --q(1) <= b;
    --q(2) <= c;
    --q(3) <= d;

end arch;