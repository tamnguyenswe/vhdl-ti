library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity akkumulator is 

    port (
        d   : in std_logic_vector(3 downto 0); -- Daten-Eingang
        ld  : in std_logic; -- MUX Steuereingang: 0(gedrückt)=Ergebnis durchschalten, 1(nicht gedrückt)= Dateneingang durchschalten
        sub : in std_logic; -- 0=+, 1=-
        e   : in std_logic; -- enable Auffangregister 0(gedrückt)= enabled
        clk : in std_logic; -- TakttT

        c   : out std_logic; -- LEDG(4) carry out
        q   : out std_logic_vector(3 downto 0); -- LEDG(3..0) Daten-Ausgang

        -- debug pins:
        z_out : out std_logic_vector(4 downto 0)    -- LEDR(4..0), z4 = carry
        );
end entity akkumulator;

architecture Arch of akkumulator is
signal q_int : unsigned(3 downto 0);
--signal x : unsigned(3 downto 0);
--signal y : unsigned(3 downto 0);
signal z : unsigned(4 downto 0);

begin
    P:process(clk)
    begin
        if (falling_edge(clk) and (e = '0')) then
            if (ld = '1') then
                q_int <= unsigned(d);
            else
                q_int <= z(3 downto 0);
            end if;

            if (sub = '1') then 
                z <= ('0' & q_int) - unsigned('0' & d);
            else
                z <= ('0' & q_int) + unsigned('0' & d);
            end if ;

        end if;
    end process P;

    c <= z(4);
    z_out <= std_logic_vector(z);
    q <= std_logic_vector(q_int);
end Arch;