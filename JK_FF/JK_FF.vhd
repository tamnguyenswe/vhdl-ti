library ieee;
use ieee.std_logic_1164.all;

entity JK_FF is 
    port (c,r,s,j,k : in std_logic;
          q, qn : out std_logic);
end JK_FF;

architecture arch of JK_FF is

signal q_int : std_logic;

begin 
    P:process(r,s,c)
    begin 
        if (S = '0') then
            q_int <= '1';

        elsif (R = '0') then
            q_int <= '0';

        elsif (c'event and c = '0') then
            if (j = '1' and k = '1') then
                q_int <= not q_int;
            elsif (j = '1' and k = '0') then
                q_int <= '1';
            elsif (j = '0' and k = '1') then
                q_int <= '0';
            end if;
        end if;                            
    end process P;

    q <= q_int;
    qn <= not q_int;

end arch;