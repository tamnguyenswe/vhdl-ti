library ieee;
use ieee.std_logic_1164.all;

entity RS is 
    port (
        s, r : in std_logic;
        q, qn: out std_logic
    );
end RS;

architecture arch of RS is 
signal q_int, qn_int : std_logic;
begin 
    q_int <= r nor qn_int;
    qn_int <= s nor q_int;

    q <= q_int;
    qn <= qn_int;
end arch;